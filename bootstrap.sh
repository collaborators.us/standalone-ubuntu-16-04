!/usr/bin/env bash

HERE=$(pwd)

# Fix username regex
sed -i '/NAME_REGEX/ a NAME_REGEX="^[a-z][-.a-z0-9_]*$"' /etc/adduser.conf


# Force SSH key-based login
sed -i 's/^\s*PasswordAuthentication.*$/PasswordAuthentication no/' /etc/ssh/sshd_config
sed -i 's/^\s*RSAAuthentication.*$/RSAAuthentication yes/' /etc/ssh/sshd_config
sed -i 's/^\s*PasswordAuthentication.*$/PasswordAuthentication yes/' /etc/ssh/sshd_config


# Install docker engine
sudo apt-get remove docker docker-engine docker.io containerd runc
sudo apt-get update

sudo apt-get install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

if ! apt-key fingerprint 0EBFCD88 | grep -q "9DC8 5822 9FC7 DD38 854A  E2D8 8D81 803C 0EBF CD88"; then
    echo "WARNING! Docker key fingerprint doesn't match. Aborting!"
    exit 1;
fi

echo "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker-ce.list

sudo apt-get update && sudo apt-get install -y docker-ce docker-ce-cli containerd.io

sudo usermod -aG docker $USER

sudo systemctl enable docker


# settings
echo '{
  "log-driver": "json-file",
  "log-opts": {
    "max-size": "10m",
    "max-file": "10"
  }
}' | sudo tee /etc/docker/daemon.json


# docker-compose
sudo curl -L "https://github.com/docker/compose/releases/download/1.24.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

sudo chmod +x /usr/local/bin/docker-compose


# Deploy traefik to support multiple local containers sharing a single set of web ports
mkdir -p /data/traefik/config
touch /data/traefik/config/acme.json
docker-compose -f $HERE/docker/traefik.yml -p traefik up -d


# Install the Gitlab runner
read -p "Install the Gitlab runner? " -n 1 -r
echo    # move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]; then
    GITLAB_RUNNER=gitlab-runner
    sudo useradd --comment 'GitLab Runner' --create-home $GITLAB_RUNNER --shell /bin/bash
    sudo usermod -aG docker $GITLAB_RUNNER
    sudo wget -O /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64
    sudo chmod +x /usr/local/bin/gitlab-runner
    sudo gitlab-runner install --user=$GITLAB_RUNNER --working-directory=/home/$GITLAB_RUNNER
    sudo gitlab-runner start
fi


# tmux, vim
sudo apt-get update && sudo apt-get install -y tmux vim


# Add user instructions:
echo
echo "Create a user:"
echo "# adduser <username>"
echo
echo "Add the user to the sudo and docker groups:"
echo "# usermod -aG sudo,docker <username>"
echo
echo "Add the user's public SSH key:"
echo "# su - <username>"
echo "# mkdir -p \$HOME/.ssh"
echo "# echo \"<THE-USERS-PUBLIC-KEY>\" >> \$HOME/.ssh/authorized_keys"
echo "# chmod 700 \$HOME/.ssh"
echo "# chmod 600 \$HOME/.ssh/authorized_keys"
echo "# exit"

