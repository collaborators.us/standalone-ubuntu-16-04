# Bootstrap a new standalone Ubuntu 16.04 server

## Credit

Author: Nate Morris
Date: May 21, 2019
Modified May 21, 2019

## Prerequisites

- An Ubuntu 16.04 system
- Root access to the system

## First Steps

1. Clone this repo
   - `apt-get install -y git`
   - `git clone git@gitlab.com:collaborators.us/standalone-ubuntu-16-04.git`

2. Run `bootstrap.sh`

